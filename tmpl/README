Vmod_%{MODULE} README
Copyright (C) %{YEAR} %{REALNAME}
See the end of file for copying conditions.

* Introduction

This file contains brief information about configuring, testing
and using vmod_%{MODULE}. It is *not* intended as a replacement
for the documentation, and is provided as a brief reference only.
For accessing complete documentation, please see the section
'Documentation' below.

* Overview

* Installation

In order to compile the package you need to have installed
varnishd and varnishapi package.

Supposing that condition is met, run:

  ./configure

It should be able to automatically find the necessary components. In case 
it doesn't, tweak the configuration variables as necessary. The most 
important one is PKG_CONFIG_PATH, which contains a path (in the UNIX sense)
where the .pc files are located. It should contain a directory where the
'varnishapi.pc' file lives. Example usage:

  ./configure PKG_CONFIG_PATH=/usr/local/varnish/lib/pkgconfig:$PKG_CONFIG_PATH

Please read the file INSTALL for a detailed discussion of available variables
and command line options.

Once configured, do

  make

This will build the module.  After this step you can optionally run
'make test' to test the package.

Finally, run the following command as root:

  make install

* Documentation

The manual page vmod_%{MODULE}(3) will be available after a successful
install.  To read it without actually installing the module, run
`man src/vmod_%{MODULE}.3'.

An online copy of the documentation is available from
%{URL}.

* Bug reporting

Send bug reports and suggestions to <%{EMAIL}>


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
